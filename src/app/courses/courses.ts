export class Courses {
   icon:string;
   description:string;

   constructor(icon:string, description:string) {
      this.icon = icon;
      this.description = description;
   }
}