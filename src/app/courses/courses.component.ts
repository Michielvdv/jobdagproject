import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { StudentService } from '../services/student.service';
import { Courses } from './courses';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
courses: Courses[];
  constructor(private courseService: StudentService) {
    this.courses = this.courseService.getCourses();
   }

  ngOnInit(): void {
  }
  
  getCourses():Courses[] {
    return this.courses;
  }
}
