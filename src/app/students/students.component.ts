import { Component, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';
import { Student } from './Student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  students :Student[];
  constructor(private studentService: StudentService) {
    
   }

  ngOnInit(): void {
    this.students=  this.studentService.getStudents();
  }

  getStudents():Student[] {
    return this.students;
  }

}
