import { Injectable } from '@angular/core';
import { Courses } from '../courses/courses';
import { Student } from '../students/Student';


@Injectable({
  providedIn: 'root'
})
export class StudentService {
private students :Student[]= [
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 1"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 2"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 3"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 4"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 5"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 6"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 7"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 8"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 9"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 10"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 11"
),
new Student(
  "",
  "../../assets/IMG_7778.jpg",
  "Random naam 12"
),
]

private courses :Courses[] = [
  new Courses (
    "logos:java",
    "JAVA SE 11"
  ),
  new Courses (
    "logos:javascript",
    "Javascript"
  ),
  new Courses (
    "logos:angular-icon",
    "Angular"
  ),
  new Courses (
    "logos:html-5",
    "HTML 5"
  ),
  new Courses (
    "logos:css-3",
    "CSS"
  ),
  new Courses (
    "logos:hibernate",
    "Hibernate"
  ),
  new Courses (
    "logos:spring-icon",
    "Spring"
  ),
  new Courses (
    "vscode-icons:file-type-maven",
    "Maven"
  ),
  new Courses (
    "whh:ajax",
    "Ajax"
  ),
  new Courses (
    "logos:json",
    "JSON"
  ),
  new Courses (
    "logos:bootstrap",
    "Bootstrap"
  ),
  new Courses (
    "carbon:sql",
    "SQL"
  ),
  new Courses (
    "cib:mysql",
    "MySQL"
  ),
  new Courses (
    "vscode-icons:file-type-jsp",
    "JSP"
  ),
  new Courses (
    "logos:tomcat",
    "Tomcat"
  ),
  new Courses (
    "vscode-icons:file-type-rest",
    "REST"
  )
];
  constructor() {

   }

getStudents():Student[] {
  return this.students;
}

getCourses():Courses[] {
  return this.courses;
}
}
