import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { DescriptionComponent } from './description/description.component';
import { FormComponent } from './form/form.component';
import { StudentsComponent } from './students/students.component';

const routes: Routes = [
  {
    path: "Home",
    component: DescriptionComponent,
  },
  {
    path: "Courses",
    component: CoursesComponent
  },
  {
    path: "Students",
    component: StudentsComponent
  },
  {
    path: "Contact",
    component: FormComponent,
  },
  {
    path: "",
    redirectTo: "Home", pathMatch: "full",
    
  },
  
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{anchorScrolling:'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
