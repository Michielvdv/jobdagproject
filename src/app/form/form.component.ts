import { formatCurrency } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {  FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  myForm:FormGroup;
  constructor(private fb:FormBuilder) {
    this.myForm=fb.group({
      name:['',Validators.required],
      companyName:['',Validators.required],
      email:['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      subject:[''],

    })
   }

  ngOnInit(): void {
  }

  sendForm():void{
    console.log(
      "name: "+ this.myForm.controls.name.value+"\n" +
      "companyName: "+this.myForm.controls.companyName.value+"\n" +
      "email: "+this.myForm.controls.email.value+"\n" +
      "subject: "+this.myForm.controls.subject.value+"\n"
    )
  }
}
