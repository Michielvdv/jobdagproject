import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigatie',
  templateUrl: './navigatie.component.html',
  styleUrls: ['./navigatie.component.scss']
})
export class NavigatieComponent implements OnInit {
  displayClass:string;
  linkText:string;
  constructor(private route:Router) { 
    this.displayClass="none";
    this.linkText= "▼";
  }

  ngOnInit(): void {
  }

  changeDisplay():void{
    if(this.displayClass!=="none"){
      this.displayClass="none";
      this.linkText= "▼";
    }else{
      this.displayClass="block";
      this.linkText= "▲";
    }
  }

  goToHome() {
    this.route.navigate(['/Home'], {fragment:'welcome'});
    this.changeDisplay();
  }
  goToCourses() {
    this.route.navigate(['/Courses'], { fragment: 'main' });
    this.changeDisplay();
  }
  goToDescription() {
    this.route.navigate(['/Home'], {fragment:'main'});
    this.changeDisplay();
  }
  goToStudents() {
    this.route.navigate(['/Students'], { fragment: 'main' });
    this.changeDisplay();
  }
  goToContact() {
    this.route.navigate(['/Contact'], { fragment: 'main' });
    this.changeDisplay();
  }
}
